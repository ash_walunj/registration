import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;
import java.sql.PreparedStatement;
import java.util.Objects;


public class Ui1 implements ActionListener
{
    JFrame frame;
    JPanel panel;
    JTextField name;
    JTextField number;
    JTextField email;
    JLabel user,num,mail,crs;
    JComboBox course;
    JButton button,clear;
    int n;
    String nm,course_selected,mail_;
    Ui1()
    {
        String arr[]={"PCM","PCB","PCMB"};
        frame=new JFrame("Registration Window");
        panel=new JPanel();
        name=new JTextField(10);
        number=new JTextField(10);
        email=new JTextField(10);
        user=new JLabel("Name:");
        num=new JLabel("Number:");
        mail=new JLabel("E-mail");

        button=new JButton("Register");
        clear=new JButton("Clear");
        crs=new JLabel("Course");
        course=new JComboBox(arr);
        button.addActionListener(this);
        clear.addActionListener(this);
        panel.add(user);
        panel.add(name);
        panel.add(num);
        panel.add(number);
        panel.add(mail);
        panel.add(email);
        panel.add(crs);
        panel.add(course);
        panel.add(button);
        frame.add(panel);
        frame.setSize(250,250);
        frame.setVisible(true);

    }

    @Override
    public void actionPerformed(ActionEvent obj)
    {
        if(obj.getSource()==clear)
        {
            name.setText(" ");
            email.setText(" ");
            number.setText(" ");
        }
       if(obj.getSource()==button)
       {
           n= Integer.parseInt(number.getText());
           nm=name.getText();
           mail_=email.getText();
           course_selected= String.valueOf(course.getItemAt(course.getSelectedIndex()));
           System.out.println(n+" "+nm+" "+mail_+" "+course_selected);
           JOptionPane.showMessageDialog(frame, "Registered");
           try {
               put();
           } catch (ClassNotFoundException e) {
               e.printStackTrace();
           } catch (SQLException e) {
               e.printStackTrace();
           }
       }
    }

    void put()throws ClassNotFoundException,SQLException
    {
        Class.forName("oracle.jdbc.driver.OracleDriver");
        Connection con= DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE","ashwin","aw12067");
        DatabaseMetaData dbmd=con.getMetaData();
        Statement stmt=con.createStatement();
        PreparedStatement pr=con.prepareStatement("insert into regestired1 values(?,?,?,?)");
        pr.setString(1, nm);
        pr.setString(2, String.valueOf(n));
        pr.setString(3, mail_);
        pr.setString(4,course_selected);
        pr.execute();
        ResultSet rs=stmt.executeQuery("select * from regestired1");
        ResultSetMetaData rsmd=rs.getMetaData();
        while (rs.next())
        {
            System.out.println(rs.getString(1)+" "+rs.getString(2)+" "+rs.getString(4));

        }
    }


}
