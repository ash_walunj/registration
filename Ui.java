import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;

public class Ui implements ActionListener {
    JFrame frame;
    JPanel panel;
    JTextField username;
    JPasswordField password;
    JLabel user, pass;
    JButton button;
    String un1, pss1;
    int c;

    Ui()
    {
        frame = new JFrame("Login Window");
        panel = new JPanel();
        username = new JTextField(10);
        password = new JPasswordField(10);
        user = new JLabel("Username:");
        pass = new JLabel("Password:");
        button = new JButton("Login");
        button.addActionListener(this);
        panel.add(user);
        panel.add(username);
        panel.add(pass);
        panel.add(password);
        panel.add(button);
        frame.add(panel);
        frame.setSize(250, 250);
        frame.setVisible(true);
    }


    @Override
    public void actionPerformed(ActionEvent obj) {
        if (obj.getSource() == button) {
            un1 = username.getText();
            pss1 = password.getText();
            try {
                check();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            if (c == 1) {
                System.out.println("User authorized");
                frame.dispose();
                JOptionPane.showMessageDialog(frame, "Login completed");
                new Ui1();
            } else {
                System.out.println("Wrong username or password");
            }

        }
    }

    void check() throws ClassNotFoundException, SQLException {
        Class.forName("oracle.jdbc.driver.OracleDriver");
        Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE", "ashwin", "aw12067");
        DatabaseMetaData dbmd = con.getMetaData();
        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery("select * from login");
        ResultSetMetaData rsmd = rs.getMetaData();
        while (rs.next()) {
            if (rs.getString(1).equals(un1)) {
                if (rs.getString(2).equals(pss1)) {
                    c = 1;
                    break;
                } else {
                    c = 0;
                }
            }
        }
    }
}


