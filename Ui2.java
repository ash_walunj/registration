import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;
import java.sql.PreparedStatement;
import java.util.Objects;

public class Ui2 implements ActionListener {
    JFrame frame;
    JPanel panel;
    JTextField username;
    JPasswordField password;
    JPasswordField confirm_password;
    JLabel user,pass,con_pass;
    JButton button,log;
    String un,pss;
    Ui2()
    {
        frame=new JFrame("SignUp Window");
        panel=new JPanel();
        username=new JTextField(10);
        password=new JPasswordField(10);
        confirm_password=new JPasswordField(10);
        user=new JLabel("Username:");
        pass=new JLabel("Password:");
        con_pass=new JLabel("Confirm Password");
        button=new JButton("SignUp");
        log=new JButton("Login");
        button.addActionListener(this);
        panel.add(user);
        panel.add(username);
        panel.add(pass);
        panel.add(password);
        panel.add(con_pass);
        panel.add(confirm_password);
        panel.add(button);
        panel.add(log);
        frame.add(panel);
        frame.setSize(400,250);
        frame.setVisible(true);
    }
    public static void main(String[] args)
    {
        new Ui2();
    }
    @Override
    public void actionPerformed(ActionEvent obj)
    {

        if(obj.getSource()==button)
        {
            if(Objects.equals(password.getText(), confirm_password.getText()))
            {
                System.out.println("Password Matched");
                un=username.getText();
                pss=password.getText();
                frame.dispose();
                try
                {
                    put();
                }
                catch (ClassNotFoundException e)
                {
                    e.printStackTrace();
                }
                catch (SQLException e)
                {
                    e.printStackTrace();
                }
                JOptionPane.showMessageDialog(frame, "Sign Up succesful");
            }
            else
            {
                System.out.println("Inccorect Password");
            }
        }
        if(obj.getSource()==log)
        {
            new Ui();
        }


    }
    void put()throws ClassNotFoundException,SQLException
    {
        Class.forName("oracle.jdbc.driver.OracleDriver");
        Connection con= DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE","ashwin","aw12067");
        DatabaseMetaData dbmd=con.getMetaData();
        Statement stmt=con.createStatement();
        PreparedStatement pr=con.prepareStatement("insert into login values(?,?)");
        pr.setString(1, un);
        pr.setString(2,pss);
        pr.execute();
        ResultSet rs=stmt.executeQuery("select * from login");
        ResultSetMetaData rsmd=rs.getMetaData();
        while (rs.next())
        {
            System.out.println(rs.getString(1)+" "+rs.getString(2));

        }
    }
}